<?php

namespace Drupal\twitter_feeds\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Twitter feeds Block.
 *
 * @Block(
 *   id="twitter_feeds",
 *   admin_label = @Translation("Twitter Feeds"),
 * )
 */
class TwitterFeedsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'twitter_feeds' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['twitter_feeds'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Twitter feeds configuration'),
      '#description' => $this->t('Configure twitter feeds'),
      '#prefix' => '<div id="items-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['twitter_feeds']['twitter_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter username'),
      '#default_value' => $config['twitter_username'] ?? 'drupal',
      '#attributes' => ['class' => ['twitter-username']],
      '#description' => $this->t('Twitter username without @ symbol. Eg. drupal'),
      '#required' => TRUE,
    ];

    $form['twitter_feeds']['widget_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter widget id'),
      '#default_value' => $config['widget_id'] ?? '',
      '#attributes' => ['class' => ['twitter-widget-id']],
      '#description' => $this->t("Twitter widget ID is the 18-digit number that you see right after widgets on your twitter account edit page. You will use it to add a Twitter Portlet on your WebCMS site. You can leave this field empty if you can't get the widget id."),
    ];

    $form['twitter_feeds']['tweet_limit'] = [
      '#type' => 'number',
      '#min' => '1',
      '#max' => '120',
      '#title' => $this->t('Tweet limit'),
      '#default_value' => $config['tweet_limit'] ?? '5',
      '#attributes' => ['class' => ['tweet-limit']],
      '#description' => $this->t('Number of tweets that will be displayed on the front'),
      '#required' => TRUE,
    ];

    $form['twitter_feeds']['tweets_board_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tweets board width'),
      '#default_value' => $config['tweets_board_width'] ?? '580px',
      '#attributes' => ['class' => ['tweets-board-width']],
      '#description' => $this->t('Width of the tweets container. eg. 580px'),
    ];

    $form['twitter_feeds']['feeds_layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Feeds layout'),
      '#description' => $this->t('Twitter feeds layout.'),
      '#options' => [
        'transparent' => $this->t('Transparent'),
        'light' => $this->t('Light'),
        'dark' => $this->t('Dark'),
      ],
      '#default_value' => $config['feeds_layout'] ?? 'light',
    ];

    $form['twitter_feeds']['placeholder_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder text'),
      '#description' => $this->t('Show placeholder text until the twitter feeds are loading. eg. Tweets by @drupal'),
      '#default_value' => $config['placeholder_text'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['twitter_feeds']) {
      $twitter_feeds = $values['twitter_feeds'];
      $twitter_username = $twitter_feeds['twitter_username'];
      $widget_id = $twitter_feeds['widget_id'];
      $tweet_limit = $twitter_feeds['tweet_limit'];
      $tweets_board_width = $twitter_feeds['tweets_board_width'];
      $feeds_layout = $twitter_feeds['feeds_layout'];
      $placeholder_text = $twitter_feeds['placeholder_text'];

      $this->configuration['twitter_username'] = $twitter_username;
      $this->configuration['widget_id'] = $widget_id;
      $this->configuration['tweet_limit'] = $tweet_limit;
      $this->configuration['tweets_board_width'] = $tweets_board_width;
      $this->configuration['feeds_layout'] = $feeds_layout;
      $this->configuration['placeholder_text'] = $placeholder_text;

    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $twitter_feeds_data = [
      'twitter_username' => ($config['twitter_username']) ?? "",
      'widget_id' => ($rgb_widget_id) ?? '',
      'tweet_limit' => ($config['tweet_limit']) ?? '5',
      'tweets_board_width' => ($config['tweets_board_width']) ?? '500px',
      'feeds_layout' => ($config['feeds_layout']) ?? 'light',
      'placeholder_text' => ($config['placeholder_text']) ?? '',
    ];

    $build = [];
    $build['twitter_feeds'] = [
      '#theme' => 'twitter_feeds',
      '#twitter_feeds_data' => $twitter_feeds_data,
    ];
    $build['#attributes']['class'][] = 'twitter-feeds-block';

    $build['#attached']['library'][] = 'twitter_feeds/twitter_feeds';
    return $build;
  }

}
