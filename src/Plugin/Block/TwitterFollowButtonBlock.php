<?php

namespace Drupal\twitter_feeds\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Twitter feeds Block.
 *
 * @Block(
 *   id="twitter_follow_button",
 *   admin_label = @Translation("Twitter Follow Button"),
 * )
 */
class TwitterFollowButtonBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'twitter_follow_button' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['twitter_follow_button'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Twitter follow button configuration'),
      '#description' => $this->t('Configure twitter follow button'),
      '#prefix' => '<div id="items-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['twitter_follow_button']['twitter_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter username'),
      '#default_value' => $config['twitter_username'] ?? '',
      '#attributes' => ['class' => ['twitter-username']],
      '#description' => $this->t('Twitter username without @ symbol.'),
      '#required' => TRUE,
    ];

    $form['twitter_follow_button']['show_followers_count'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show followers count'),
      '#default_value' => $config['show_followers_count'] ?? FALSE,
      '#attributes' => ['class' => ['twitter-followers-count']],
      '#description' => $this->t('Show followers count with button.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['twitter_follow_button']) {
      $twitter_follow_button = $values['twitter_follow_button'];
      $twitter_username = $twitter_follow_button['twitter_username'];
      $show_followers_count = $twitter_follow_button['show_followers_count'];

      $this->configuration['twitter_username'] = $twitter_username;
      $this->configuration['show_followers_count'] = $show_followers_count;

    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $twitter_follow_button_data = [
      'twitter_username' => ($config['twitter_username']) ?? "",
      'show_followers_count' => (isset($config['show_followers_count']) && $config['show_followers_count'] == '1') ? TRUE : FALSE,
    ];

    $build = [];
    $build['twitter_follow_button'] = [
      '#theme' => 'twitter_follow_button',
      '#twitter_follow_button_data' => $twitter_follow_button_data,
    ];

    $build['#attributes']['class'][] = 'twitter-follow-button-block';

    $build['#attached']['library'][] = 'twitter_feeds/twitter_feeds';
    return $build;
  }

}
